<?xml version="1.0" encoding="iso-8859-1"?>
<iso:schema    xmlns="http://purl.oclc.org/dsdl/schematron" 
	       xmlns:iso="http://purl.oclc.org/dsdl/schematron"
	       xmlns:nrgxsl="http://nrg.wustl.edu/validate" 
	       queryBinding='xslt2'
	       schemaVersion="ISO19757-3">
<iso:ns uri="java:org.nrg.validation.CCFHCPShimValidator" prefix="shimVal"/>
<iso:let name="shimValInst" value="shimVal:new($experimentfilename)"/>

  <iso:title>Protocol Validator</iso:title>
<!-- It is expected that each rule file would have the following Patterns available viz. Start, ListScanIds -->
  <iso:pattern id="Start">
	   <iso:title>Validation report</iso:title>
	    <iso:rule context="/">
	     	<iso:assert test="xnat:MRSession">The root element must be an MRSession</iso:assert>
	    </iso:rule>
	    <iso:rule context="xnat:MRSession">
		<iso:report id="expt_id" test="true()"><iso:value-of select="@ID"/></iso:report>
		<iso:report id="expt_project" test="true()"><iso:value-of select="@project"/></iso:report>
		<iso:report id="expt_label" test="true()"><iso:value-of select="@label"/></iso:report>
	    </iso:rule>
 </iso:pattern>
<iso:pattern id="Acquisition">
	<iso:rule context="/">
		<iso:assert test="shimVal:CheckFieldmapFollowsLocalizer($shimValInst)"> 
			<nrgxsl:acquisition>
				<nrgxsl:cause-id>Localizer Blocks Followed By Fieldmap Check</nrgxsl:cause-id>
				<iso:value-of select="./shimVal:CheckFieldmapFollowsLocalizerText($shimValInst)" disable-output-escaping="yes"/>
			</nrgxsl:acquisition> 
		</iso:assert>
       </iso:rule>
</iso:pattern>  
<iso:pattern id="ListScanIds">
	    <iso:rule context="//xnat:scan">
		<iso:report test="@ID">
		  <nrgxsl:scans>
		    <iso:value-of select="@ID"/>
		  </nrgxsl:scans> 	
		</iso:report>
            </iso:rule>
 </iso:pattern>
 <iso:pattern id="Scan">
  <!-- ************************************************************************************************** -->
  <!-- ***********************************************  3T SCANS **************************************** -->
  <!-- ************************************************************************************************** -->

  <!-- ***********************************************  COMMON **************************************** -->
  		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='SpinEchoFieldMap_PA']">
  			<iso:assert test="xnat:frames = 1" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>						
  			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
  			<iso:assert test="xnat:parameters/xnat:flip = 90" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '1.99' and @x &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '1.99' and @y &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '1.99' and @z &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 65 and xnat:parameters/xnat:te &lt; 67" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (65ms,67ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>						
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  		</iso:rule>
 		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='SpinEchoFieldMap_AP']">
  			<iso:assert test="xnat:frames = 1" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>						
  			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
  			<iso:assert test="xnat:parameters/xnat:flip = 90" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '1.99' and @x &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '1.99' and @y &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '1.99' and @z &lt; '2.1']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (1.99mm,2.1mm) Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 65 and xnat:parameters/xnat:te &lt; 67" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (65ms,67ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>						
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  		</iso:rule>
 		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='PCASL_SpinEchoFieldMap_PA']">
  			<iso:assert test="xnat:frames = 1" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '36'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 36 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>						
  			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
  			<iso:assert test="xnat:parameters/xnat:flip = 90" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@x='384']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@y= '384']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '3.4' and @x &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.4mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '3.4' and @y &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.5mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '3.4' and @z &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.4mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 39 and xnat:parameters/xnat:te &lt; 41" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (39ms,41ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2894 and xnat:parameters/xnat:pixelBandwidth &lt; 2896" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (2894Hz/px,2896Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000479 and xnat:parameters/xnat:echoSpacing &lt;  0.000491" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (0.000479,0.000491) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>						
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  		</iso:rule>  		
  		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='PCASL_SpinEchoFieldMap_AP']">
  			<iso:assert test="xnat:frames = 1" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '36'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 36 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>						
  			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
  			<iso:assert test="xnat:parameters/xnat:flip = 90" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@x='384']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:fov[@y= '384']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '3.4' and @x &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.4mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '3.4' and @y &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.5mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '3.4' and @z &lt; '3.6']" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
  			   <iso:value-of select="concat('Expected: (3.4mm,3.6mm) Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 39 and xnat:parameters/xnat:te &lt; 41" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (39ms,41ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2894 and xnat:parameters/xnat:pixelBandwidth &lt; 2896" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (2894Hz/px,2896Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000479 and xnat:parameters/xnat:echoSpacing &lt;  0.000491" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (0.000479,0.000491) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>						
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
  		</iso:rule> 		
 		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='PCASLhr_SpinEchoFieldMap_PA']">
   			<iso:assert test="xnat:frames = 1" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
   			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '60'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 60 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>						
   			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
   			<iso:assert test="xnat:parameters/xnat:flip = 90" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:fov[@x='688']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:fov[@y= '688']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@x = '2.5']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.5']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt;= '2.26' and @z &lt;= '2.28']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: [2.26mm,2.28mm] Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:te &gt; 39 and xnat:parameters/xnat:te &lt; 41" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: (39ms,41ms) Found: ', ./xnat:parameters/xnat:te)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2324 and xnat:parameters/xnat:pixelBandwidth &lt; 2326" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (2324Hz/px,2326Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt;= 0.000569 and xnat:parameters/xnat:echoSpacing &lt;=  0.000571" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: [0.000569,0.000571] Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>						
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
   		</iso:rule>  		
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='PCASLhr_SpinEchoFieldMap_AP']">
   			<iso:assert test="xnat:frames = 1" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
   			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '60'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 60 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>						
   			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
   			<iso:assert test="xnat:parameters/xnat:flip = 90" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:fov[@x='688']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:fov[@y= '688']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@x='2.5']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@y='2.5']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt;= '2.26' and @z &lt;= '2.28']" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
   			   <iso:value-of select="concat('Expected: [2.26mm,2.28mm] Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:te &gt; 39 and xnat:parameters/xnat:te &lt; 41" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: (39ms,41ms) Found: ', ./xnat:parameters/xnat:te)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:tr &gt; 7999 and xnat:parameters/xnat:tr &lt;  8001" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (7999ms,8001ms) Found: ', ./xnat:parameters/xnat:tr)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2324 and xnat:parameters/xnat:pixelBandwidth &lt; 2326" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (2324Hz/px,2326Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
   			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt;= 0.000569 and xnat:parameters/xnat:echoSpacing &lt;=  0.000571" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: [0.000569,0.000571] Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
 			   <nrgxsl:scan>
 			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
 			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
 			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
 				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
 			   </nrgxsl:scan>
 			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>						
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
   		</iso:rule>  		
 <!-- ***********************************************  END OF COMMON **************************************** -->  
  <!-- ***********************************************  STRUCTURAL **************************************** -->
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T1w_setter' and contains(xnat:parameters/xnat:imageType,'MOSAIC')]">
			<iso:assert test="xnat:frames &gt;= 166 and xnat:frames &lt;= 196 " >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: [166,196] volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 8" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='192']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 192 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '192']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 192 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '7.99' and @x &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '7.99' and @y &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '7.99' and @z &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 9.8 and xnat:parameters/xnat:tr &lt;=  11.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (9.8ms,11.0ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 4.5 and xnat:parameters/xnat:te &lt;= 4.8" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (4.5ms,4.8ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;= 490 and xnat:parameters/xnat:pixelBandwidth &lt; 521" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (490Hz/px,521Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 3000.0 or xnat:parameters/xnat:readoutSampleSpacing = 3200.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3000.0 or 3200.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.70] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'BC'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: BC Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="shimVal:CheckImageComments($shimValInst, @ID)">
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>ImageComments</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: No invalid characters Found: ', ./shimVal:CheckImageCommentsText($shimValInst, @ID))"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T1w_setter' and not(contains(xnat:parameters/xnat:imageType,'MOSAIC'))]">
			<iso:assert test="xnat:frames = 32 " >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 32 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 2" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='32']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 32 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '32']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 32 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '7.99' and @x &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '7.99' and @y &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '7.99' and @z &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 9.8 and xnat:parameters/xnat:tr &lt;=  11.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (9.8ms,11.0ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 4.5 and xnat:parameters/xnat:te &lt;= 4.8" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (4.5ms,4.8ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;4884 and xnat:parameters/xnat:pixelBandwidth &lt; 5211" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (4884Hz/px,5211Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 3000.0 or xnat:parameters/xnat:readoutSampleSpacing = 3200.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3000.0 or 3200.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.44 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.44" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.44,0.44] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'BC'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: BC Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T1w_MPR_vNav_4e']">
			<iso:assert test="xnat:frames = 832" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 832 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 8" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='300']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 300 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '320']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 320 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '0.79' and @x &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '0.79' and @y &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '0.79' and @z &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 2499.0 and xnat:parameters/xnat:tr &lt;  2501.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2499.0ms,2501.0ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='MultiEcho_TE1']) = '1.81'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>MultiEcho_TE1</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: 1.81 Found: ', ./xnat:parameters/xnat:addParam[@name='MultiEcho_TE1'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='MultiEcho_TE2']) = '3.6'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>MultiEcho_TE2</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: 3.6 Found: ', ./xnat:parameters/xnat:addParam[@name='MultiEcho_TE2'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='MultiEcho_TE3']) = '5.39'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>MultiEcho_TE3</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: 5.39 Found: ', ./xnat:parameters/xnat:addParam[@name='MultiEcho_TE3'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='MultiEcho_TE4']) = '7.18'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>MultiEcho_TE4</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: 7.18 Found: ', ./xnat:parameters/xnat:addParam[@name='MultiEcho_TE4'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>				
			<iso:assert test="xnat:parameters/xnat:ti = 1000.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TI</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.ti</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1000.0 Found: ', ./xnat:parameters/xnat:ti)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;744 and xnat:parameters/xnat:pixelBandwidth &lt; 746" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (744Hz/px,746Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 2100.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 2100.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.70] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T1w_MPR_vNav_4e RMS' or xnat:series_description='T1w_MPR_vNav_4e_RMS']">
			<iso:assert test="xnat:frames = 208" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 208 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			 			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 8" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='300']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 300 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '320']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 320 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '0.79' and @x &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '0.79' and @y &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '0.79' and @z &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 2499.0 and xnat:parameters/xnat:tr &lt;  2501.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2499.0ms,2501.0ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:ti = 1000.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TI</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.ti</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1000.0 Found: ', ./xnat:parameters/xnat:ti)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te  &gt;1.80 and xnat:parameters/xnat:te &lt; 1.82" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1.81 Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;744 and xnat:parameters/xnat:pixelBandwidth &lt; 746" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (744Hz/px,746Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 2100.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 2100.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.70] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T2w_setter' and not(contains(xnat:parameters/xnat:imageType,'MOSAIC'))]">
			<iso:assert test="xnat:frames = 32" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 32 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 2" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='32']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 32 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '32']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 32 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '7.99' and @x &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '7.99' and @y &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '7.99' and @z &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 12.0 and xnat:parameters/xnat:tr &lt; 14.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (12ms,14ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 5.0 and xnat:parameters/xnat:te &lt; 7.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (5.0ms,7.0ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;4884 and xnat:parameters/xnat:pixelBandwidth &lt; 5211" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (4884Hz/px,5211Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 3000.0 or xnat:parameters/xnat:readoutSampleSpacing = 3200.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3000.0 or 3200.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.44 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.44" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.44,0.44] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>						
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>			
			 <iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'BC'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: BC Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T2w_setter' and contains(xnat:parameters/xnat:imageType,'MOSAIC')]">
			<iso:assert test="xnat:frames &gt;= 97 and xnat:frames &lt;= 122" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: [97,122] volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 120" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 120 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='192']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 192 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '192']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 192 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '7.99' and @x &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '7.99' and @y &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '7.99' and @z &lt; '8.1']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 8.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 12.0 and xnat:parameters/xnat:tr &lt; 14.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (12ms,14ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 5.0 and xnat:parameters/xnat:te &lt; 7.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (5.0ms,7.0ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;= 490 and xnat:parameters/xnat:pixelBandwidth &lt; 521" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (490Hz/px,521Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 3000.0 or xnat:parameters/xnat:readoutSampleSpacing = 3200.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3000.0 or 3200.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt; -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt; 0.70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (-0.175,0.70) Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>			
			 <iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'BC'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: BC Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckImageComments($shimValInst, @ID)">
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>ImageComments</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="concat( 'Expected: No invalid characters Found: ', ./shimVal:CheckImageCommentsText($shimValInst, @ID))"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='T2w_SPC_vNav']">
			<iso:assert test="xnat:frames = 208" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 208 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Sag'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Sagittal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Sag')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Sag'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Sag Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 120" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 120 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='300']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 300 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '320']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 320 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '0.79' and @x &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '0.79' and @y &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt; '0.79' and @z &lt; '0.81']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 0.8mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 3199.0 and xnat:parameters/xnat:tr &lt; 3201.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (3199ms,3201ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 563.0 and xnat:parameters/xnat:te &lt; 565.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (563.0ms,565.0ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;744 and xnat:parameters/xnat:pixelBandwidth &lt; 746" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (744Hz/px,746Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 2100.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 2100.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.70] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>		
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='TSE_HiResHp']">
			<iso:assert test="xnat:frames = 30" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 30 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:orientation = 'Cor'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Coronal orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Cor')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Cor Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'CorgtTra(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Cor&gt;Tra(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 135" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 135 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='384']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '384']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x &gt; '0.38' and @x &lt; '0.4']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: (0.38mm,0.4mm) Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y &gt; '0.38' and @y &lt; '0.4']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: (0.38mm,0.4mm) Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt; 4799.0 and xnat:parameters/xnat:tr &lt; 4801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (4799ms,4801ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 105.0 and xnat:parameters/xnat:te &lt; 107.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (105.0ms,107.0ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;129 and xnat:parameters/xnat:pixelBandwidth &lt; 131" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (129Hz/px,131Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:readoutSampleSpacing = 10000.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Readout SampleSpacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.readoutSampleSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 10000.0 Found: ', ./xnat:parameters/xnat:readoutSampleSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='ROW'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: ROW Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
<!-- *********************************************** END STRUCTURAL **************************************** -->
<!-- ***********************************************  FUNCTIONAL FNCA **************************************** -->
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='rfMRI_REST_AP']">
			<iso:assert test="xnat:frames = 488 or xnat:frames = 263" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: (488 or 263) volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='rfMRI_REST_PA']">
			<iso:assert test="xnat:frames = 488 or xnat:frames = 263" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: (488 or 263) volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='rfMRI_REST_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
    			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='rfMRI_REST_AP_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
    			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    				
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
<!-- *********************************************** END FUNCTIONAL FNCA **************************************** -->
<!-- ***********************************************   DIFFUSION **************************************** -->
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir98_AP']">
			<iso:assert test="xnat:frames = 99" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 99 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			  			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:bMax = 3005.0 or xnat:parameters/xnat:diffusion/xnat:bMax = 3010.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion bMax</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.bMax</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3005.0 or 3010.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:bMax)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir98_PA']">
			<iso:assert test="xnat:frames = 99" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 99 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
      			   <nrgxsl:scan>
      			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
      			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
      			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
      				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
      			   </nrgxsl:scan>
      			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:bMax = 3005.0 or xnat:parameters/xnat:diffusion/xnat:bMax = 3010.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion bMax</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.bMax</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3005.0 or 3010.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:bMax)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir98_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volume Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir98_AP_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volume Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir99_AP']">
			<iso:assert test="xnat:frames = 100" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 100 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3221ms, 3223ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:bMax = 3005.0 or xnat:parameters/xnat:diffusion/xnat:bMax = 3010.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion bMax</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.bMax</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3005.0 or 3010.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:bMax)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir99_PA']">
			<iso:assert test="xnat:frames = 100" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 100 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:bMax = 3005.0 or xnat:parameters/xnat:diffusion/xnat:bMax = 3010.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion bMax</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.bMax</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 3005.0 or 3010.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:bMax)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir99_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volume Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='dMRI_dir99_AP_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volume Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '92'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 92 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 78" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 78 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle = 160.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Diffusion RefocusFlipAngle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.diffusion.refocusFlipAngle</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 160.0 Found: ', ./xnat:parameters/xnat:diffusion/xnat:refocusFlipAngle)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '1400']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1400 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '1.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 1.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;=  3229.0 and xnat:parameters/xnat:tr &lt;=  3231.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [3229ms, 3231ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt;= 88 and xnat:parameters/xnat:te &lt;= 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [88ms,90ms] Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;1699 and xnat:parameters/xnat:pixelBandwidth &lt; 1701" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (1699Hz/px,1701Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000689 and xnat:parameters/xnat:echoSpacing &lt;  0.000691" >
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
  				<iso:value-of select="concat('Expected: (0.000689,0.000691) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert>      			
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
<!-- *********************************************** END DIFFUSION **************************************** -->
<!-- *********************************************** BEGIN tfMRI **************************************** -->

		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_FACENAME_PA']">
			<iso:assert test="xnat:frames = 345" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 345 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_FACENAME_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_CARIT_PA']">
			<iso:assert test="xnat:frames = 300" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 300 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
     			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    						
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_CARIT_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_CARIT_AP']">
			<iso:assert test="xnat:frames = 300" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 300 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
     			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    						
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_CARIT_AP_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_VISMOTOR_PA']">
			<iso:assert test="xnat:frames = 194" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 194 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_VISMOTOR_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   			      <iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
    			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='mbPCASL_PA']">
			<iso:assert test="xnat:frames = 70" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 70 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '36'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 36 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
    			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
			<iso:assert test="xnat:parameters/xnat:flip = 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='384']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '384']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 384 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='3.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 3.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '3.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 3.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '3.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 3.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  3459.0 and xnat:parameters/xnat:tr &lt;  3461.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (3459ms,3461ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 13 and xnat:parameters/xnat:te &lt; 14" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (13ms,14ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2894 and xnat:parameters/xnat:pixelBandwidth &lt; 2896" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2894Hz/px,2896Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000489 and xnat:parameters/xnat:echoSpacing &lt;  0.000491" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (0.000489,0.000491) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="abs(xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation) &gt;= 2.97 and abs(xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation) &lt;= 3.32 " >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected:  [2.97, 3.32] OR [-3.32,-2.97]   Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='mbPCASLhr_PA']">
			<iso:assert test="xnat:frames = 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 90 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '60'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 60 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
    			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'] != 'Tra'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>		
			<iso:assert test="xnat:parameters/xnat:flip = 90" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 90 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='688']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '688']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 688 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.5']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.5mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z &gt;= '2.26' and @z &lt;= '2.28']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: [2.26mm,2.28mm] Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  3579.0 and xnat:parameters/xnat:tr &lt;  3581.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (3579ms,3581ms) Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 18 and xnat:parameters/xnat:te &lt; 20" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (18ms,20ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2324 and xnat:parameters/xnat:pixelBandwidth &lt; 2326" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2324Hz/px,2326Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt;= 0.000569 and xnat:parameters/xnat:echoSpacing &lt;=  0.000571" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [0.000569,0.000571] Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="abs(xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation) &gt;= 2.97 and  abs(xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation) &lt;= 3.32" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected:  [2.97, 3.32] OR [-3.32,-2.97]   Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimAdvancedMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Advanced Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimAdvancedModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
<!-- *********************************************** BEGIN tfMRI CCF_HCD_ITK Projects scans **************************************** -->
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_GUESSING_PA']">
			<iso:assert test="xnat:frames = 280" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 280 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
  			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_GUESSING_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_GUESSING_AP_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_GUESSING_AP']">
			<iso:assert test="xnat:frames = 280" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 280 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='1'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 1 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_EMOTION_PA_SBRef']">
			<iso:assert test="xnat:frames = 1" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 1 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='tfMRI_EMOTION_PA']">
			<iso:assert test="xnat:frames = 178" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Frames</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>frames</nrgxsl:xmlpath>
			      <iso:value-of select="concat( 'Expected: 178 volumes Found: ', ./xnat:frames)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count']) = '72'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Mosaic Slice Count</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 72 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Mosaic Slice Count'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>			
			<iso:assert test="xnat:parameters/xnat:orientation = 'Tra'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Orientation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.orientation</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: Axial orientation Found: ', ./xnat:parameters/xnat:orientation)"/>
			   </nrgxsl:scan>
			</iso:assert>
   			<iso:assert test="starts-with(normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text']), 'Tra')" >
       			   <nrgxsl:scan>
       			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
       			      <nrgxsl:cause-id>Siemens Orientation Text</nrgxsl:cause-id>
       			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
       				<iso:value-of select="concat('Expected: Starts with Tra Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
       			   </nrgxsl:scan>
       			</iso:assert>			    			    			
  			<iso:assert test="replace(xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'],'[^A-Za-z0-9\-\.()]+','') != 'TragtCor(-16.0)'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Rotated by AutoAlign</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: Not Tra&gt;Cor(-16.0) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Orientation Text'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>	
			<iso:assert test="xnat:parameters/xnat:flip = 52" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Flip Angle</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.flip</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 52 Found: ', ./xnat:parameters/xnat:flip)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@x='936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Row</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:fov[@y= '936']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Column</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.fov.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 936 Found: ', ./xnat:parameters/xnat:fov/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@x ='2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[x]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.x</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@x)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@y = '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Voxel Resolution[y]</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.y</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@y)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:voxelRes[@z= '2.0']" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Slice Thickness</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.voxelRes.z</nrgxsl:xmlpath>
			   <iso:value-of select="concat('Expected: 2.0mm Found: ', ./xnat:parameters/xnat:voxelRes/@z)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:tr &gt;  799.0 and xnat:parameters/xnat:tr &lt;  801.0" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TR</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.tr</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [799ms,801ms] Found: ', ./xnat:parameters/xnat:tr)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:te &gt; 36 and xnat:parameters/xnat:te &lt; 38" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>TE</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.te</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (36ms,38ms) Found: ', ./xnat:parameters/xnat:te)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:pixelBandwidth &gt;2289 and xnat:parameters/xnat:pixelBandwidth &lt; 2291" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Bandwidth</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.pixelBandwidth</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: (2289Hz/px,2291Hz/px) Found: ', ./xnat:parameters/xnat:pixelBandwidth)"/>
			   </nrgxsl:scan>
			</iso:assert>
 			<iso:assert test="xnat:parameters/xnat:echoSpacing &gt; 0.000579 and xnat:parameters/xnat:echoSpacing &lt;  0.000581" >
    			   <nrgxsl:scan>
    			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
    			      <nrgxsl:cause-id>Echo Spacing</nrgxsl:cause-id>
    			      <nrgxsl:xmlpath>parameters.echoSpacing</nrgxsl:xmlpath>
    				<iso:value-of select="concat('Expected: (0.000579,0.000581) Found: ', ./xnat:parameters/xnat:echoSpacing)"/>
    			   </nrgxsl:scan>
    			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction ='COL'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Direction</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.direction</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: COL Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:direction)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &gt;= -0.175 and  xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation &lt;= 0.175" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding Rotation</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.rotation</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: [-0.175,0.175] Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:rotation)"/>
			   </nrgxsl:scan>
			</iso:assert>
			<iso:assert test="xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive ='0'" >
			   <nrgxsl:scan>
			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
			      <nrgxsl:cause-id>Phase Encoding DirectionPositive</nrgxsl:cause-id>
			      <nrgxsl:xmlpath>parameters.inPlanePhaseEncoding.directionPositive</nrgxsl:xmlpath>
				<iso:value-of select="concat('Expected: 0 Found: ', ./xnat:parameters/xnat:inPlanePhaseEncoding/xnat:directionPositive)"/>
			   </nrgxsl:scan>
			</iso:assert> 
 			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Table Position']) = '0\0\0'" >
   			   <nrgxsl:scan>
   			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
   			      <nrgxsl:cause-id>Table position</nrgxsl:cause-id>
   			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
   				<iso:value-of select="concat('Expected: 0\0\0 Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Table Position'])"/>
   			   </nrgxsl:scan>
   			</iso:assert>
  			<iso:assert test="normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'HEA;HEP' or normalize-space(xnat:parameters/xnat:addParam[@name='Siemens Coil String']) = 'FL;FR;HL;HR'" >
     			   <nrgxsl:scan>
     			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
     			      <nrgxsl:cause-id>Coil String</nrgxsl:cause-id>
     			      <nrgxsl:xmlpath>parameters/addParam</nrgxsl:xmlpath>
     				<iso:value-of select="concat('Expected: (HEA;HEP or FL;FR;HL;HR) Found: ', ./xnat:parameters/xnat:addParam[@name='Siemens Coil String'])"/>
     			   </nrgxsl:scan>
     			</iso:assert>			
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>
		<iso:rule context="/xnat:MRSession/xnat:scans/xnat:scan[xnat:series_description='GEPCI_HCP']">
			<iso:assert test="shimVal:CheckShimStandardMode($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Shim Check Standard Mode</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckShimStandardModeText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
			<iso:assert test="shimVal:CheckReferenceVoltage($shimValInst, @ID)"> 
  			   <nrgxsl:scan>
  			      <nrgxsl:scan-id><iso:value-of select="@ID"/></nrgxsl:scan-id>
  			      <nrgxsl:cause-id>Reference Voltage Check</nrgxsl:cause-id>
  			      <nrgxsl:xmlpath>NA</nrgxsl:xmlpath>
  			      <iso:value-of select="./shimVal:CheckReferenceVoltageText($shimValInst, @ID)"/>
  			   </nrgxsl:scan>
  			</iso:assert>
		</iso:rule>

  </iso:pattern>
</iso:schema>
